	AREA	GPIO, CODE, READWRITE	
	EXPORT lab4
		
	EXTERN make_num
	EXTERN make_chars
	EXTERN output_string
	EXTERN read_string
	EXTERN output_character
	EXTERN read_character
	EXTERN enter
	EXTERN div_and_mod
	EXTERN uart_init
	EXTERN pin_connect_block_setup_for_uart0
	EXTERN display_digit_on_7_seg
	EXTERN read_from_push_btns
	EXTERN illuminateLEDs
	EXTERN Illuminate_RGB_LED
			
	
prompt	= "\r\n------------------------------------------------------------------------------------------------------------------------------\r\nWelcome to lab #4. Please select an option from below by entering the specified number or character followed by pressing ENTER.\r\n\r\n",0   	; Text to be sent to PuTTy
option0 = "----MAIN MENU----\r\nEnter '0' to enter an hexadecimal digit to be displayed in binary on the LEDs.\r\n",0
option1 = "Enter '1' to read a binary value from the momentary push buttons and display the decimal equivalent to the screen.\r\n",0
option2 = "Enter '2' to enter an hexadecimal digit to be displayed in hexadecimal on the 7-segment display.\r\n",0
option3 = "Enter '3' to choose a color to illuminate the RGB LED or to turn it off.\r\n",0
option4 = "Enter 'q' (lowercase) to quit the program.\r\n",0

menu_input = "\0",0

led_prompt = "\r\n\r\nPlease enter a hexadecimal digit between '0' and 'F' followed by pressing ENTER.\r\nIf 'A' through 'F' is entered, enter it in UPPERCASE. Your hexadecimal input will be displayed\r\non the LEDs in binary. The leftmost LED (labeled P1.16) is the Most Significant Bit (MSB)\r\nand the rightmost LED (P1.19) is the Least Signifcant Bit (LSB). (e.g., entering the\r\ncharacter 'E' will result in 1 1 1 0 on the LEDs from Left to Right.)\r\n",0
led_input = "\0",0

button_prompt = "\r\n\r\nUse the push buttons to enter a binary number. Button labeled P1.20 is the MSB, while button P1.23 is the LSB.\r\nDepressing a button corresponds to logical 1, while a button not depressed corresponds to logical 0.\r\nYou need to hold down all the buttons you wish to set 1s for at the same time, then press ENTER\r\nwhile still holding the buttons down to set the values.\r\n(e.g., Hold down left-most button P1.20 and press ENTER to enter 1000 (8 decimal).)\r\n",0
button_done = "The number you entered (given in decimal) is: ",0

seg_prompt = "\r\n\r\nPlease enter a hexadecimal digit between '0' and 'F' followed by pressing ENTER. If 'A' through 'F' is entered, enter it in UPPERCASE.\r\nYour hexadecimal input will be displayed in hexadecimal on the 7-segment display.\r\n",0
seg_input = "\0",0

rgb_prompt = "\r\n\r\nPlease select from the options below to set the color of the RGB LED. Press ENTER after typing your selection.\r\n\r\n",0
rgb_input = "\0",0

rgb_option0 = "Enter '0' for red.\r\n",0
rgb_option1 = "Enter '1' for green.\r\n",0
rgb_option2 = "Enter '2' for blue.\r\n",0
rgb_option3 = "Enter '3' for purple.\r\n",0
rgb_option4 = "Enter '4' for yellow.\r\n",0
rgb_option5 = "Enter '5' for white.\r\n",0
rgb_option6 = "Enter '6' to turn the RGB LED off.\r\n",0

exit_prompt = "\r\n\r\nYou have EXITED the program!\r\n\r\n",0


	ALIGN
		
; This lookup table stores the ASCII characters corresponding to
; decimal values for 0 through 15 inclusive. They are used to display
; the decimal equivalent of the binary input from the momentary
; push buttons to the screen in PUTTY.
; The order of the ASCII chars appears reversed due to Little Endianness
; of the ARM architecture.  When stored to memory, the least significant
; byte is stored at the word address.  When we access the data, we do
; so byte-by-byte starting at his word address, thus necessitating "reversing" them here.	
push_button_NUMS
			DCD 0x00000030	; 0
			DCD 0x00000031	; 1
			DCD 0x00000032	; 2
			DCD 0x00000033	; 3
			DCD 0x00000034	; 4
			DCD 0x00000035	; 5
			DCD 0x00000036	; 6
			DCD 0x00000037	; 7
			DCD 0x00000038	; 8
			DCD 0x00000039	; 9
			DCD 0x00003031	; 10
			DCD 0x00003131	; 11
			DCD 0x00003231	; 12
			DCD 0x00003331	; 13
			DCD 0x00003431	; 14
			DCD 0x00003531	; 15

	ALIGN
		
lab4
				STMFD SP!,{lr}	; Store register lr on stack
				
				; INITIALIZATION CODE:
			
				; 7-seg: g segment lighted
				MOV r0, #16			  		; The 17th entry in the digits_SET lookup table corresponds to the g-segment lighted (pin 13)
											; This is the offset to access that value.

				STMFD SP!, {r0-r11}			; spill registers to stack
				BL display_digit_on_7_seg
				LDMFD SP!, {r0-r11}			; spill registers to stack
				
				; LEDs:	 ALL lit
				MOV r0, #0xF				; Value 0xF lights up all LEDs
				STMFD SP!, {r0-r11}			; spill registers to stack
				BL illuminateLEDs
				LDMFD SP!, {r0-r11}			; spill registers to stack
				
				; RGB LED: WHITE
				MOV r0, #5				   	; The 5th entry in the rgb_COLORS lookup table corresponds to the value
											; to set RGB to white. This is the offset.

				STMFD SP!, {r0-r11}			; spill registers to stack
				BL Illuminate_RGB_LED
				LDMFD SP!, {r0-r11}			; spill registers to stack
				
				LDR r4, =prompt				; Load initial prompt
				BL output_string			; Print prompt			

continue_lab4	
				LDR r4, =option0			; Load Option 0
				BL output_string			; Print prompt
			
				LDR r4, =option1			; Load Option 1
				BL output_string			; Print prompt

				LDR r4, =option2			; Load Option 2
				BL output_string			; Print prompt
			
				LDR r4, =option3			; Load option 3
				BL output_string			; Print prompt
			
				LDR r4, =option4			; Load option 4
				BL output_string			; Print prompt
			
				LDR r4, =menu_input			; Load menu_input address
				STMFD SP!, {r0-r11}			; spill registers to stack
				BL read_string				; Read user input
				LDMFD SP!, {r0-r11}			; spill registers to stack
		
				LDRB r1, [r4]				; Load input byte

				CMP r1, #0x30				; Check if input is '0' - display binary value on LEDs
				BEQ leds

				CMP r1, #0x31				; Check if input is '1' - read value from push buttons
				BEQ push_buttons

				CMP r1, #0x32				; Check if input is '2' - display HEX digit on 7-seg display
				BEQ seven_seg

				CMP r1, #0x33				; Check if input is '3' - Illuminate or turn off RGB LED
				BEQ rgb_led				

				CMP r1, #0x71				; Check if input is 'q' - QUIT PROGRAM
				BEQ end_func				; User selected 'q' for quit. Goto end_func


				
leds			LDR r4, =led_prompt	 		; Load prompt address
				BL output_string			; Display prompt
				LDR r4, =led_input			; Load address to where input will be stored in memory
				STMFD SP!, {r0-r11}			; spill registers to stack
				BL read_string				; take in user input (ASCII value of HEX digit)
				LDMFD SP!, {r0-r11} 		; restore registers from stack
					
				LDRB r0, [r4] 				; Load least significant byte of led_input to r0 (user input)
				SUB r0, r0, #0x30			; shift digit in range '0' - '9' to 0x0 - 0x9
				CMP r0, #9					; Compare r0 to 9. If r0 > 9, then r0 is not digit 0-9, but is A-F instead
				ADDGT r0, r0, #0x30			; If r0 > 9, restore original ASCII HEX digit by + 0x30
				SUBGT r0, r0, #0x37			; Shift HEX digit from 'A' - 'F' (0x41 - 0x46) to 0xA - 0xF [note: 0x41 - 0x37 = 0xA == 10 decimal]
				STMFD SP!, {r0-r11}			; spill registers to stack
				BL illuminateLEDs			; light up appropriate LEDs for binary representation of HEX digit input
				LDMFD SP!, {r0-r11} 		; restore registers from stack	
				BL enter
				B continue_lab4				; Loop back to lab4
				

push_buttons	LDR r4, =button_prompt		; Load address button_prompt
				BL output_string			; Display button_prompt
				STMFD SP!, {r1-r11}			; spill registers to stack
				BL read_from_push_btns		; Take in and store binary input from push buttons, returning value entered (in decimal) in r0
				LDMFD SP!, {r1-r11} 		; restore registers from stack
						
				LDR r4, =button_done		; Load address of button_done prompt
				STMFD SP!, {r0-r11}			; spill registers to stack
				BL output_string			; Display prompt
				LDMFD SP!, {r0-r11}			; load registers from stack
				MOV r0, r0, LSL #2			; Multiply num entered on buttons by 4 to generate address offset for lookup table
				LDR r4, =push_button_NUMS	; Load base address of lookup table
				ADD r4, r4, r0				; Add offset to base address
				BL output_string			; Output bytes stored at address r4 in lookup table as ASCII characters to screen
				BL enter
				B continue_lab4				; Loop back to lab4
				
				
seven_seg		LDR r4, =seg_prompt			; load address of seg_prompt string
				BL output_string			; display seg_prompt
				LDR r4, =seg_input			; load base address to which user input is stored in memory
				STMFD SP!, {r0-r11}			; spill registers to stack
				BL read_string				; get user input as ASCII HEX digit for 7-seg display
				LDMFD SP!, {r0-r11} 		; restore registers from stack	

				LDRB r0, [r4]				; load least significant byte from seg_input into r0
				SUB r0, r0, #0x30			; shift digit in range '0' - '9' to 0x0 - 0x9
				CMP r0, #9					; Compare r0 to 9. If r0 > 9, then r0 is not digit 0-9, but is A-F instead
				ADDGT r0, r0, #0x30			; If r0 > 9, restore original ASCII HEX digit by + 0x30
				SUBGT r0, r0, #0x37			; Shift HEX digit from 'A' - 'F' (0x41 - 0x46) to 0xA - 0xF [note: 0x41 - 0x37 = 0xA == 10 decimal]
				STMFD SP!, {r0-r11}			; spill registers to stack
				BL display_digit_on_7_seg	; light up 7-seg with appropriate digit
				LDMFD SP!, {r0-r11} 		; restore registers from stack
				BL enter
				B continue_lab4				; Loop back to lab4
				
rgb_led			LDR r4, =rgb_prompt			; Load and display rgb_prompt address
				BL output_string

				LDR r4, =rgb_option0		; Load and display rgb options
				BL output_string
				LDR r4, =rgb_option1
				BL output_string
				LDR r4, =rgb_option2
				BL output_string
				LDR r4, =rgb_option3
				BL output_string
				LDR r4, =rgb_option4
				BL output_string
				LDR r4, =rgb_option5
				BL output_string
				LDR r4, =rgb_option6
				BL output_string

				LDR r4, =rgb_input			; Load address to which user ASCII input is stored (will be '0' - '6')
				STMFD SP!, {r0-r11}			; spill registers to stack
				BL read_string				; get user input
				LDMFD SP!, {r0-r11} 		; restore registers from stack	

				LDRB r0, [r4]				; load least significant byte from rgb_input address (user input)
				SUB r0, r0, #0x30			; shift digit in range '0' - '9' to 0x0 - 0x9
				STMFD SP!, {r0-r11}			; spill registers to stack
				BL Illuminate_RGB_LED		; display appropriate color or turn off RGB LED
				LDMFD SP!, {r0-r11} 		; restore registers from stack
				BL enter
				B continue_lab4				; Loop back to lab4
				
end_func		
				LDR r4, =exit_prompt		; Load address of exit prompt and display
				BL output_string
				

				LDMFD SP!, {lr}	; Restore register lr from stack	
				BX LR
	END